﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene
            {
                Eesnimi = "Henn",
                Perenimi = "Sarv",
                Sünniaeg = new DateTime(1955, 03, 07)
            };
            henn.Prindi();

            Console.WriteLine(henn.Vanus());


        }
    }
}
