﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        public static object Source { get; private set; }

        static void Main(string[] args)
        {


            //    Console.WriteLine("Hello World! It is Tuesday! Waiting for Friday!");
            //Console.Write("Kes sa oled: ");



            //string nimi = Console.ReadLine();
            //string vastus = $"Tere {nimi}!";
            //Console.WriteLine(vastus);



            //int a = 3;
            //int b = 10;
            //int c;
            //Console.WriteLine(c = a * b);
            //Console.WriteLine(+c); // ??



            //int a = 1;
            //int b = 1;
            //int c = 3;
            //a = c;
            //Console.WriteLine(a == b);
            //Console.WriteLine(b == c);



            //int x1 = 10;
            //int x2 = 20;
            //int y1 = ++x1;
            //Console.WriteLine(x1);
            //Console.WriteLine(y1);
            //int y2 = x2++;
            //Console.WriteLine(x2);
            //Console.WriteLine(y2);



            //int a = 18%3;
            //int b = 19%3;
            //int c = 20%3;
            //int d = 21%3;
            //Console.WriteLine(a);
            //Console.WriteLine(b);
            //Console.WriteLine(c);
            //Console.WriteLine(d);



            //byte a = 255;
            //int d = (a + 1);
            //Console.WriteLine(d);



            //string nimi = "Henn";
            //string teinenimi = "Sarv";
            //string täisnimi = nimi + " " + teinenimi;
            //Console.WriteLine(täisnimi);
            //string lause = "Luts kirjutab:\n\t\r \"Kui Arno isaga ...\" ja nii edasi";
            //string veellause = @"Luts kirjutab ""Kui Arno isaga ..."" ja nii edasi";
            //string filename = "C:\\henn\\text.txt";
            //string teinefilename = @"C:\Users\student\source\repos\tere-maailm";
            //Console.WriteLine(lause);



            //string nimi = "Henn";
            //string teinenimi = "Sarv";
            //nimi = nimi + " " + teinenimi;
            //Console.WriteLine(nimi);

            //teinenimi = "Henn Sarv";
            //Console.WriteLine(nimi == teinenimi);
            //Console.WriteLine(nimi.Equals(teinenimi)); //Javas PEAB nii tegema, meil on ka ilus nii
            //Console.WriteLine(nimi.Equals(teinenimi, StringComparison.CurrentCultureIgnoreCase));
            ////Equalsiga saab võrrelda mitut moodi
            ////Equals on objektidel üleüldine võrdlemise funktsioon



            //string nimi = "Henn";
            //string teinenimi = "Sarv";
            //nimi = nimi + " " + teinenimi;
            //Console.WriteLine(nimi);
            //int vanus = 64;
            //Console.WriteLine(nimi + " on " + vanus + " aastat vana");  // 1. liitmine (halb)
            //Console.WriteLine("{0} on {1} aastat vana", nimi, vanus);   // 2. formaatimisega


            //string teade = String.Format("{0} on {1} aastat vana", nimi, vanus);
            //Console.WriteLine(teade);                // 3. formaatimisfunktsiooniga

            //Console.WriteLine("{0} on tore poiss kuigi {0} on {1} aastat juba vana", nimi, vanus);

            //teade = string.Format("{0} on {1} aastat vana", nimi, vanus);
            //string teade2 = $"{nimi} on {vanus} aastat vana";
            //// need kaks lauset on identsed!!!
            //Console.WriteLine(teade);
            //Console.WriteLine(teade2);

            //Console.WriteLine(nimi);
            //Console.WriteLine(nimi.ToUpper());
            //Console.WriteLine(nimi.ToLower());
            ////stringiga saab teha hulga teised
            ////teisendada suureks või väikseks täheks
            //Console.WriteLine(nimi.Split(' ')[1]); //teha tükkideks
            //Console.WriteLine(nimi.Substring(5, 4)); // võtta jupikesi NB! JAVAS jälle teisiti

            //string nimi = "Vadim";
            //string perenimi = "Malõšev";
            //int vanus = 44;
            //Console.WriteLine("{0} {1} on {2} aastat vana", nimi, perenimi, vanus);
            //string teade = $"{nimi} {perenimi} on {vanus} aastat vana";
            //Console.WriteLine(teade); // kaks võimalust

            //string selline = "see on reavahetusega \n string";
            //Console.WriteLine(selline);



            //    if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday) // NB! ne stavit tochku s zapjatoi!!!
            //    {
            //        //neid lauseid siin täisetakse vaid kolmapäeviti
            //        Console.WriteLine("Jee! nädala keskel oleme");
            //        int a = 3;
            //        Console.WriteLine(a);
            //    }
            //    else if (DateTime.Now.DayOfWeek == DayOfWeek.Friday) // need laused täidetakse reedel
            //        Console.WriteLine("kohe saab maale jooma minna");

            //    else

            //        // neid lause täidetakse AINULT muudel päevadel
            //        Console.WriteLine("igav");
            //    string a = "Ahaa";
            //    Console.WriteLine(a);

            //    Console.WriteLine("tavaline päev");
            //    // siit edasi juba iga päev
            //    Console.WriteLine("veel üks aga kavalam lause - switch");
            //}



            //switch (DateTime.Now.DayOfWeek)
            //{
            //    case DayOfWeek.Wednesday:
            //    case DayOfWeek.Friday:
            //        // kolmapäevased ja reedesed toimetused
            //        Console.WriteLine("läheme trenni");
            //        break;
            //    case DayOfWeek.Saturday:
            //        Console.WriteLine("läheme sauna");
            //        // laupäevased toimetused
            //        goto case DayOfWeek.Sunday;
            //    case DayOfWeek.Monday:
            //        Console.WriteLine("väike peaparandus");
            //        goto default;
            //    case DayOfWeek.Sunday:
            //        // siia pühapäevased asjad
            //        Console.WriteLine("joome õlut");
            //        break;
            //    default:
            //        // muude päevade toimetused
            //        Console.WriteLine("teeme tööd");
            //        break;
            //}


            //Console.Write("Mis värvi valgusfoor on: ");
            //string värv = Console.ReadLine().ToLower();
            //            if (värv == "roheline" || värv == "green") Console.WriteLine("Sõida edasi");
            //else if (värv == "kollane" || värv == "yellow") Console.WriteLine("Oota rohelist");
            //else if (värv == "punane" || värv == "red") Console.WriteLine("jää seisma");
            //else { Console.WriteLine("osta prillid"); }

            //switch (Console.ReadLine().ToLower()) // poprobovat napisat samomu



            //int arv = 7; // muutujal on 1 väärtus igal hetkel
            //// vahel tahaks rohkem

            //int[] arvud = new int[10]; // sellel muutujal võib olla terve rida väärtusi
            //// ja seda kutsitakse massiiviks

            //arvud = new int[10]; // nüüd on sellel asjal on 10 väärtust
            //arvud[4] = 78; //viies neist on nüüd 78
            //arvud[3]++; // neljas neist on nüüd 1

            //int[] teised1 = new int[5] { arv, arv * 2, arv * 3, arv * 4, 7 };
            //int[] teised2 = new int[] { arv, arv * 2, arv * 3, arv * 4, 7 };
            //int[] teised3 = { arv, arv * 2, arv * 3, arv * 4, 7 };

            //arvud = new int[] { 1, 2, 3 };

            //arvud = teised2;
            //teised2[0]++;
            //Console.WriteLine(arvud[0]);

            //Console.WriteLine("massivis on {0} arvu", arvud.Length);

            //int[][] seeOnImelikloom = { new int[3], new int[7], new int[10] };

            //int[,] tabel = new int[8, 8];
            //Console.WriteLine(tabel.Length);
            //Console.WriteLine(tabel.Rank);
            //Console.WriteLine(tabel.GetLength(0));
            //Console.WriteLine(tabel.GetLength(1));



            //int[] esimene = { 1, 2, 3 };
            //int[] teine = { 4, 5, 6 };
            //esimene = esimene.Union(teine).ToAray();
            //Console.WriteLine(esimene);



            //int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //for (int i = 0; i < arvud.Length; i++) // for (initiator, terminator, iterator)
            //{
            //    Console.WriteLine(arvud[i]); // täidetav blokk
            //}


            //int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            //for (int i = 0; i < arvud.Length; i++)
            //{
            //    Console.WriteLine($"arv number {i} on {arvud[i]}"); // dlja chego znak dollara???
            //}


            //int[] arvud = { 7,2,3,8,9,1,4,8,9 };

            //for (int i = arvud.Length; i > 0; )
            //{
            //    Console.WriteLine(arvud[--i]);
            //}
            //            foreach (int x in arvud)
            //{
            //    Console.WriteLine(x);
            //}


            // var kasutatakse, kui rase öelda mis tüüpi see on
            //int arv = 7;
            //Console.WriteLine(arv.GetType().Name);

            //var nimi = "Henn Sarv";
            //Console.WriteLine(nimi.GetType().Name);


            //Console.Write("Tead mis, ");
            //string vastus = "";
            ////for (; vastus != "ostan";)
            //while (vastus != "ostan")
            //{
            //    Console.Write("osta elevant ära: ");
            //    vastus = Console.ReadLine();

            //    Console.Write($"Kõik ütlevad, et {vastus}, aga ");
            //}
            //Console.WriteLine("tubli, elevant on nüüd sinu");


            //Console.Write("Mis värvi valgusfoor on? ");
            //string värv = Console.ReadLine().ToLower();
            //while (värv == "roheline")
            //{
            //    Console.WriteLine("Sõida edasi");
            //    break;
            //}
            //while (värv == "kollane")
            //{
            //    Console.WriteLine("Oota rohelist");
            //    break;
            //}
            //while (värv == "punane")
            //{
            //    Console.WriteLine("Jää seisma");
            //    break;
            //}
            //while (värv == "")
            //{
            //    Console.WriteLine("osta prillid");
            //}


            //bool oota = true;
            //while (oota)
            //{
            //    Console.Write("Mis värvi valgusfoor on: ");
            //    switch (Console.ReadLine().ToLower())
            //    {
            //        case "green":
            //        case "roheline":
            //            Console.WriteLine("Sõida edasi");
            //            oota = false;
            //            break;
            //        case "yellow":
            //        case "kollane":
            //            Console.WriteLine("Oota rohelist");
            //            break;
            //        case "red":
            //        case "punane":
            //            Console.WriteLine("Jää seisma");
            //            break;
            //        default:
            //            Console.WriteLine("Osta prillid");
            //            break;
            //    }
            //}



            //string[] nimed = { "Vadim", "Sergei" };
            //Console.Write("Mis sinu nimi on: ");
            //switch (Console.ReadLine().ToLower())
            //{
            //    case "Vadim":
            //        Console.WriteLine("Tere, Vadim");
            //        break;
            //    case "Sergei":
            //        Console.WriteLine("Jää seisma");
            //        break;
            //    default:
            //        break;
            //}






            //const int ÕPILASTE_ARV = 20;
            //string[] nimed = new string[ÕPILASTE_ARV];
            //int[] vanused = new int[ÕPILASTE_ARV];
            //int õpilasteArv = 0;
            //for (; õpilasteArv < ÕPILASTE_ARV; õpilasteArv++)
            //{
            //    Console.Write($"Anna õpilase nr {õpilasteArv} nimi:");
            //    nimed[õpilasteArv] = Console.ReadLine();
            //    if (nimed[õpilasteArv] == "") break;
            //    Console.Write($"Anna {nimed[õpilasteArv]} vanus: ");
            //    vanused[õpilasteArv] = int.Parse(Console.ReadLine());
            //}
            //Console.WriteLine($"õpilasi on {õpilasteArv}");

            //double vanusteSumma = 0;
            //for (int i = 0; i < õpilasteArv; i++) vanusteSumma += vanused[i];
            //double keskmineVanus = vanusteSumma / õpilasteArv;
            //Console.WriteLine($"keskmine vanus on {keskmineVanus:F2}");

            //int vanimVanus = vanused[0];
            //int kesOnVanim = 0;

            //for (int i = 1; i < õpilasteArv; i++)
            //    if (vanused[i] > vanimVanus)
            //        vanimVanus = vanused[kesOnVanim = i];

            //Console.WriteLine($"vanim on {nimed[kesOnVanim]}");




            //int i = 77;
            //double d = Math.PI;
            //DateTime täna = DateTime.Today;

            //Console.WriteLine("Meie i on " + i.ToString());
            //Console.WriteLine("meie d on " + d.ToString());
            //    Console.WriteLine("meie d on {0:F4}
            //    Console.WriteLine("
            //    Console.WriteLine("



            //Console.Write("anna üks arv (võib ka miinusega olla): ");
            //int arv = int.Parse(Console.ReadLine());
            ////arv = arv < 0 ? -arv : arv;    // või nii
            ////arv *= (arv < 0 ? -1 : 1);     // vahel on kasulikkum teha nii
            ////arv = Math.Abs(arv);           // enamasti tehakse nii
            //Console.WriteLine(arv);


            //int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };
            //int[] ruudud = new int[10];
            ////for (int i = 0; i < ruudud.Length; i++) ruudud[i] = i * i;
            ////foreach (var x in ruudud) Console.WriteLine(x);

            //List<int> arvudelist = new List<int>(100) { 1, 2, 3, 4, 5, 6, 7 };

            //arvudelist.Add(14);
            //arvudelist.Remove(4);
            //arvudelist.Remove(6);
            //arvudelist.RemoveAt(1);

            //foreach (var x in arvud) Console.WriteLine(x);
            //foreach (var x in arvudelist) Console.WriteLine(x);



            //Dictionary<string, int> nimekiri = new Dictionary<string, int>();

            //nimekiri.Add("Henn", 64);
            //nimekiri.Add("Ants", 28);
            //nimekiri.Add("Peeter", 40);

            //Console.WriteLine(nimekiri["Henn"]);

            //foreach (var x in nimekiri) Console.WriteLine(x);

            //Dictionary<string, string> sõnad = new Dictionary<string, string>()
            //{
            //    {"love", "armastus" },
            //    {"hate", "viha" },
            //    {"money", "raha" },
            //    {"cristmastree", "jõulupuu" },
            //};



            //Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            //while(true)
            //{
            //    Console.Write("Anna nimi: ");
            //    string nimi = Console.ReadLine();
            //    if (nimi == "") break;
            //    Console.Write($"anna {nimi} vanus: ");
            //    int vanus = int.Parse(Console.ReadLine());
            //    nimekiri.Add(nimi, vanus);
            //}

            //Console.WriteLine("Keskmine vanis on {0}", nimekiri.Values.Average());
            //int maxVanus = nimekiri.Values.Max();

            //foreach(var x in nimekiri)
            //    if (x.Value == maxVanus)
            //        Console.WriteLine($"Vanim on {x.Key} ta on {x.Value} aastane");



            //    var loetud = File.ReadAllText(@@"C:\Users\student\source\repos\new-project\ConsoleApp3\uusnimekiri.txt");
            //    Console.WriteLine(loetud);
            //    var loetudRead = File.ReadAllLines(@"C:\Users\student\source\repos\new-project\ConsoleApp3\uusnimekiri.txt");
            //    for (int i = 0; i < loetudRead.Length; i++)
            //    {
            //        Console.WriteLine($"rida { i}
            //    -- { loetudRead[i]}
            //    ");
            //    }

            //string uusfailinimi = @"..\..\uusnimekiri.txt";
            //File.WriteAllLines(uusfailinimi, loetudRead);


            //    Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            //    foreach (var x in loetudRead)

            //        var jupid = x.Split(' ');
            //string nimi = jupid[0];
            //int vanus = int.Parse(jupid[1]);
            //nimekiri.Add(nimi, vanus);

            //    var loetud = File.ReadAllText(C: \Users\student\source\repos\new- project\ConsoleApp3\uusnimekiri.txt);


            //string text = System.IO.File.ReadAllText(@"C:\Users\student\source\repos\new-project\ConsoleApp3\uusnimekiri.txt");
            //string[] lines = System.IO.File.ReadAllLines(@"C:\Users\student\source\repos\new-project\ConsoleApp3\uusnimekiri.txt");
            //System.Console.WriteLine("Contents of uusnimekiri.txt");
            //foreach (string line in lines)
            //{
            //    // Use a tab to indent each line of the file.
            //    Console.WriteLine("\t" + line);
            //}



            //Console.WriteLine("\nesimene variant paneme puuduvad\n");
            //int[] pakk = new int[52];
            //Random r = new Random(5138100);
            //int mitu = 0;
            //do
            //{
            //    int uus = r.Next(52);
            //    if (!pakk.Contains(uus)) pakk[mitu++] = uus;

            //}
            //while (mitu < 51);
            //for (int i = 0; i < pakk.Length; i++)
            //{
            //    Console.Write($"{(i % 4 == 0 ? "\n" : "\t")}{pakk[i]}");
            //}
            //Console.WriteLine("\n\nteine variant \"segamisega\"\n");
            //// see seal on esimene variant

            //pakk = Enumerable.Range(0, 52).ToArray(); // praegu on järjest

            //for (int i = 0; i < 10000; i++)
            //{
            //    int a = r.Next(52);
            //    int b = r.Next(52);
            //    int c = pakk[a]; pakk[a] = pakk[b]; pakk[b] = c;
            //}
            //for (int i = 0; i < pakk.Length; i++)
            //{
            //    Console.Write($"{(i % 4 == 0 ? "\n" : "\t")}{pakk[i]}");
            //}
            //Console.WriteLine("\n\nkolmas variant tõstmisega\n");

            //List<int> pakkList = Enumerable.Range(0, 52).ToList(); // pakk segamata (aga list)
            //List<int> segatudPakk = new List<int>();

            //while (pakkList.Count > 0)
            //{
            //    int a = r.Next(pakkList.Count);
            //    segatudPakk.Add(pakkList[a]);
            //    pakkList.RemoveAt(a);
            //}
            //for (int i = 0; i < segatudPakk.Count; i++)
            //{
            //    Console.Write($"{(i % 4 == 0 ? "\n" : "\t")}{segatudPakk[i]}");
            //}
            //Console.WriteLine();


            //int ridu = 8;
            //int veerge = 10;
            //int[,] arvud = new int[ridu, veerge];

            //Random r = new Random();

            //for (int i = 0; i < ridu; i++)
            //    for (int j = 0; j < veerge; j++)
            //    {
            //        arvud[i, j] = r.Next(100) + 1;
            //    }
            //for (int i = 0; i < ridu; i++)
            //{
            //    for (int j = 0; j < veerge; j++)
            //    {
            //        Console.Write($"{arvud[i, j]}\t");
            //    }
            //    Console.WriteLine();
            //}

            //for (bool kasKüsimeEdasi = true; kasKüsimeEdasi;)
            //{
            //    Console.Write("mida otsime: ");
            //    if (int.TryParse(Console.ReadLine(), out int otsitav) && otsitav <= 100 && otsitav > 0)
            //    {
            //        bool leidsin = false;
            //        for (int i = 0; i < ridu && !leidsin; i++)
            //            for (int j = 0; j < veerge && !leidsin; j++)
            //            {
            //                if (leidsin = arvud[i, j] == otsitav)
            //                {
            //                    if (arvud[i, j] == otsitav) Console.WriteLine($"leidsin {otsitav} {i + 1}-reast ja {j + 1}-veerust");
            //                    break;
            //                }
            //            }
            //        kasKüsimeEdasi = false;
            //    }

            //    else

            //    {
            //        Console.WriteLine("see pole miskine arv või pole ta vahemikus 1..100");
            //    }
            //}







        }
    }
}