﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Inimene
    {
        public string _Eesnimi;
        public string Perenimi;
        public DateTime Sünniaeg;

        public string GetEesnimi() => _AppDomain;
        public void SetEesnimi(string nimi) => _Eesnimi = TitleCase(nimi);


        public int Vanus()
        {
            return (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        }

        public void Prindi() =>
        
            Console.WriteLine($"On inimene, kelle nimi {_Eesnimi} {Perenimi}");
        
        public static string TitleCase(string nimi)
        {
            string[] nimed = nimi.Split(' ');
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i].Length == 0 ? "" :
                nimed[i] = nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }
            return string.Join(" ", nimed);
        }




    }
}

